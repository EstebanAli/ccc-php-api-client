<?php

require_once 'vendor/autoload.php';
require_once 'credentials.php';

/*
    Usage: php survey_log.php
*/

use Layer7\CCCApi\CCCClient;

$Client = new CCCClient($username, $client_id, $password, $passphrase);

// Dates to fetch from API
$date_from = '2022-04-01 00:00:00';
$date_to = '2022-04-01 23:59:59';
$instance_id = 0;//Instance id to check
$Response = $Client->SurveyLog($date_from, $date_to, $instance_id);

print_r(json_decode($Response->response));
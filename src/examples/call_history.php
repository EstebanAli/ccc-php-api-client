<?php

require_once 'vendor/autoload.php';
require_once 'credentials.php';

/*
    Usage: php call_history.php
*/

use Layer7\CCCApi\CCCClient;

$Client = new CCCClient($username, $client_id, $password, $passphrase);

// Dates to fetch from API
$date_from = '2022-04-01 00:00:00';
$date_to = '2022-04-01 23:59:59';

$date_from_name = str_replace(['-', ' ', ':'],[''], $date_from);
$date_to_name = str_replace(['-', ' ', ':'],[''], $date_to);

// Inbound file creation
$inbound_headers = ['call_id','call_direction','campaign_id','campaign_type','campaign_name','agent','callerid_number','called_number','destination','answer_state','amd_status','hangup_reason','hangup_code','hangup_code_sip','duration_secs','duration_mins','bill_time','bill_rate','bill_cost','start_datetime','answer_datetime','hangup_datetime','account_nr','tags','grp_agents_ids','grp_agents_names','list_id','lead_id','lead_encrypted_id'];
$file_inbound = fopen("IN-{$date_from_name}-{$date_to_name}.csv", 'w');
fputcsv($file_inbound, $inbound_headers);

// Outbound file creation
$outbound_headers = ['call_id','call_direction','campaign_id','campaign_type','campaign_name','agent','callerid_number','called_number','destination','answer_state','amd_status','hangup_reason','hangup_code','hangup_code_sip','duration_secs','duration_mins','bill_time','bill_rate','bill_cost','start_datetime','answer_datetime','hangup_datetime','account_nr','tags','instances_names','grp_agents_ids','grp_instances_names','list_id','lead_id','lead_encrypted_id'];
$file_outbound = fopen("OUT-{$date_from_name}-{$date_to_name}.csv", 'w');
fputcsv($file_outbound, $outbound_headers);

// Disposition file creation
$file_dispositions = fopen("Dispositions-{$date_from_name}-{$date_to_name}.csv", 'w');
$disposition_headers = ['call_id','disp_id','list_disp_id','name','description','code','text','type','status'];
fputcsv($file_dispositions, $disposition_headers);

$page = 0;
$pagesize = 1000;
$count = 0;
do {
    $page++;
    echo "Processing page {$page}.\n";

    // Consume API
    $ResponseRaw = $Client->CallHistorySearch($date_from, $date_to, $page);

    // If any HTTP error, catch it
    if ($ResponseRaw->http_response_code != '200')
    {
        print_r($ResponseRaw->response .  "\n");
        die;
    }

    $Response = json_decode($ResponseRaw->response);
    
    $Data = $Response->data;
    $count = $Data->count;
    echo "Retrieved {$count} results.\n";
    // Iterate between results
    foreach ($Data->results as $result)
    {
        $result_array = (array)$result;
        unset($result_array['dispositions']);

        if (count($result->dispositions))
        {
            foreach ($result->dispositions as $dispositions)
            {
                foreach ($dispositions as $disposition)
                {
                    foreach ($disposition as $disp)
                    {
                        $data = [
                            $result->call_id,
                            $disp->disp_id,
                            $disp->list_disp_id,
                            $disp->name,
                            $disp->description,
                            $disp->code,
                            $disp->text,
                            $disp->type,
                            $disp->status
                        ];
                        fputcsv($file_dispositions, $data);
                    }
                }
            }
        }

        fputcsv(($result->call_direction == 'inbound') ? $file_inbound : $file_outbound, $result_array);
    }
}
while($count != 0);

fclose($file_inbound);
fclose($file_outbound);
fclose($file_dispositions);
